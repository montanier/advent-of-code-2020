#!/usr/bin/python3
import argparse


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def find_seat(line):
    row = find_row(line)
    col = find_column(line)
    return row * 8 + col


def find_row(line):
    min_row = 0
    max_row = 127
    for side in list(line)[0:6]:
        if side == 'F':
            max_row = max_row - round((max_row - min_row) / 2)
        if side == 'B':
            min_row = min_row + round((max_row - min_row) / 2)
    if list(line)[6] == 'F':
        return min_row
    return max_row


def find_column(line):
    min_col = 0
    max_col = 7
    for side in list(line)[7:-1]:
        if side == 'L':
            max_col = max_col - round((max_col - min_col) / 2)
        if side == 'R':
            min_col = min_col + round((max_col - min_col) / 2)
    if list(line)[-1] == 'L':
        return min_col
    return max_col


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        max_seat_id = 0
        for line in f.readlines():
            seat_id = find_seat(line.rstrip('\n'))
            max_seat_id = max(seat_id, max_seat_id)
        print(max_seat_id)


if __name__ == "__main__":
    main()
