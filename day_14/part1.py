#!env python3
import argparse
import re

mem_pattern = re.compile('mem\[([0-9]+)\]')

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    programm = []
    for line in f.readlines():
        action, value = line.strip().split(' = ')
        if action == 'mask':
            programm.append(('ma', 0, list(value)))
        if 'mem' in action:
            mem_loc = mem_pattern.match(action).group(1)
            programm.append(('me', mem_loc, list(format(int(value), '#038b')[2:])))
    return programm


def run_mem(programm):
    mem = {}
    mask = []
    for action, mem_loc, value in programm:
        if action == 'ma':
            mask = value
        else:
            val_to_set_bin = []
            for i, x in enumerate(mask):
                if x == 'X':
                    val_to_set_bin.append(value[i])
                else:
                    val_to_set_bin.append(x)
            mem[mem_loc] = int(''.join(val_to_set_bin), 2)

    return mem


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        programm = parse_input(f)
        print(sum(run_mem(programm).values()))


if __name__ == "__main__":
    main()
