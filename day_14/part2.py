#!env python3
import argparse
import re

mem_pattern = re.compile('mem\[([0-9]+)\]')

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    programm = []
    for line in f.readlines():
        action, value = line.strip().split(' = ')
        if action == 'mask':
            programm.append(('ma', 0, list(value)))
        if 'mem' in action:
            mem_loc = mem_pattern.match(action).group(1)
            programm.append(('me', list(format(int(mem_loc), '#038b')[2:]), int(value)))
    return programm


def generate_address(mems, buff):
    if len(buff) < len(mems):
        for val in mems[len(buff):]:
            if val == 'X':
                to_return = []
                for elem in generate_address(mems, buff + ['0']):
                    to_return.append(elem)
                for elem in generate_address(mems, buff + ['1']):
                    to_return.append(elem)
                return to_return
            else:
                buff.append(val)

    return [buff]


def run_mem(programm):
    mem = {}
    mask = []
    for action, mem_loc, value in programm:
        if action == 'ma':
            mask = value
        else:
            mems = []
            for i, x in enumerate(mask):
                if x == 'X' or x == '1':
                    mems.append(x)
                else:
                    mems.append(mem_loc[i])
            for address in generate_address(mems, []):
                mem[int(''.join(address))] = value

    return mem


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        programm = parse_input(f)
        mem = run_mem(programm)
        print(sum(mem.values()))


if __name__ == "__main__":
    main()
