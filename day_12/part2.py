#!env python3
import argparse
from math import radians, cos, sin

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    for line in f.readlines():
        action = list(line)[0]
        val = int(''.join(list(line)[1:]))
        yield action, val


def step_moves(in_moves):
    delta_ew = 0
    delta_ns = 0
    waypoint_ns = 1
    waypoint_ew = 10
    for action, val in in_moves:
        if action == 'N':
            waypoint_ns += val
        if action == 'S':
            waypoint_ns -= val
        if action == 'E':
            waypoint_ew += val
        if action == 'W':
            waypoint_ew -= val
        if action == 'R':
            new_waypoint_ew = waypoint_ns * round(cos(radians(90 - val)))
            new_waypoint_ns = waypoint_ns * round(sin(radians(90 - val)))
            new_waypoint_ew += waypoint_ew * round(cos(radians(-val)))
            new_waypoint_ns += waypoint_ew * round(sin(radians(-val)))
            waypoint_ew = new_waypoint_ew
            waypoint_ns = new_waypoint_ns
        if action == 'L':
            new_waypoint_ew = waypoint_ns * round(cos(radians(90 + val)))
            new_waypoint_ns = waypoint_ns * round(sin(radians(90 + val)))
            new_waypoint_ew += waypoint_ew * round(cos(radians(val)))
            new_waypoint_ns += waypoint_ew * round(sin(radians(val)))
            waypoint_ew = new_waypoint_ew
            waypoint_ns = new_waypoint_ns
        if action == 'F':
            delta_ns += val * waypoint_ns
            delta_ew += val * waypoint_ew

        # print(f'{waypoint_ew} {waypoint_ns}')
        # print(f'{delta_ew} {delta_ns}')

    return delta_ew, delta_ns


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        parsed_input = parse_input(f)
        delta_ew, delta_ns = step_moves(parsed_input)
        print(abs(delta_ew) + abs(delta_ns))


if __name__ == "__main__":
    main()
