#!env python3
import argparse
from math import radians, cos, sin

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    for line in f.readlines():
        action = list(line)[0]
        val = int(''.join(list(line)[1:]))
        yield action, val


def step_moves(in_moves):
    delta_ew = 0
    delta_ns = 0
    orientation = 0
    for action, val in in_moves:
        # print(f'{action} {val}')
        if action == 'N':
            delta_ns += val
        if action == 'S':
            delta_ns -= val
        if action == 'E':
            delta_ew += val
        if action == 'W':
            delta_ew -= val
        if action == 'R':
            orientation -= val
        if action == 'L':
            orientation += val
        if action == 'F':
            delta_ew += val * round(cos(radians(orientation)))
            delta_ns += val * round(sin(radians(orientation)))
        # print(f'{delta_ew} {delta_ns}')

    return delta_ew, delta_ns


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        parsed_input = parse_input(f)
        delta_ew, delta_ns = step_moves(parsed_input)
        print(abs(delta_ew) + abs(delta_ns))


if __name__ == "__main__":
    main()
