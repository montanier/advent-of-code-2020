#!/usr/bin/python
import argparse
import re


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')

    return parser


def check_password_infos(file_content):
    pattern_password = re.compile("([0-9]+)-([0-9]+) ([a-zA-Z]): (.*)\n")
    good_password_count = 0
    for line in file_content:
        match_password = pattern_password.match(line)
        if match_password:
            first_position = int(match_password.group(1)) - 1
            second_position = int(match_password.group(2)) - 1
            letter = match_password.group(3)
            password = list(match_password.group(4))

            letter_count = 0
            if password[first_position] == letter:
                letter_count += 1
            if password[second_position] == letter:
                letter_count += 1
            if letter_count == 1:
                good_password_count += 1
        else:
            raise AttributeError("Did not match the regex: %s", line)

    return good_password_count


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        print(check_password_infos(f.readlines()))


if __name__ == "__main__":
    main()
