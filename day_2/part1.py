#!/usr/bin/python
import argparse
from collections import Counter
import re


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')

    return parser


def check_password_infos(file_content):
    pattern_password = re.compile("([0-9]+)-([0-9]+) ([a-zA-Z]): (.*)\n")
    good_password_count = 0
    for line in file_content:
        match_password = pattern_password.match(line)
        if match_password:
            min_count = int(match_password.group(1))
            max_count = int(match_password.group(2))
            letter = match_password.group(3)
            password = match_password.group(4)

            letters_count = Counter(list(password))
            target_letter_count = letters_count.get(letter, 0)
            if min_count <= target_letter_count <= max_count:
                good_password_count += 1
        else:
            raise AttributeError("Did not match the regex: %s", line)

    return good_password_count


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        print(check_password_infos(f.readlines()))


if __name__ == "__main__":
    main()
