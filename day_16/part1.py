#!env python3
import argparse

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    rules = {}
    own_ticket = []
    nearby_tickets = []
    state = 0
    for line in f.readlines():
        if 'your ticket' in line:
            continue
        if 'nearby tickets' in line:
            continue

        if line == '\n':
            state += 1
            continue

        line = line.strip()

        if state == 0:
            field, values = line.split(': ')
            ranges_descr = values.split(' or ')
            ranges_values = []
            for descr in ranges_descr:
                min_val, max_val = descr.split('-')
                ranges_values.append((int(min_val), int(max_val)))
            rules[field] = ranges_values

        if state == 1:
            own_ticket = line.split(',')
            own_ticket = list(map(int, own_ticket))

        if state == 2:
            ticket = line.split(',')
            nearby_tickets.append(list(map(int, ticket)))

    return rules, own_ticket, nearby_tickets


def check_validity(rules, own_ticket, nearby_tickets):
    sum_errors = 0
    for ticket in nearby_tickets:
        for value in ticket:
            valid = False
            for ranges in rules.values():
                for min_val, max_val in ranges:
                    if min_val <= int(value) <= max_val:
                        valid = True
                        break
                if valid:
                    break

            if not valid:
                sum_errors += value
    return sum_errors


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        inputs = parse_input(f)
        print(check_validity(*inputs))


if __name__ == "__main__":
    main()
