#!env python3
import argparse
from copy import deepcopy

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    rules = {}
    own_ticket = []
    nearby_tickets = []
    state = 0
    for line in f.readlines():
        if 'your ticket' in line:
            continue
        if 'nearby tickets' in line:
            continue

        if line == '\n':
            state += 1
            continue

        line = line.strip()

        if state == 0:
            field, values = line.split(': ')
            ranges_descr = values.split(' or ')
            ranges_values = []
            for descr in ranges_descr:
                min_val, max_val = descr.split('-')
                ranges_values.append((int(min_val), int(max_val)))
            rules[field] = ranges_values

        if state == 1:
            own_ticket = line.split(',')
            own_ticket = list(map(int, own_ticket))

        if state == 2:
            ticket = line.split(',')
            nearby_tickets.append(list(map(int, ticket)))

    return rules, own_ticket, nearby_tickets


def deduce_fields_nb(rules, own_ticket, nearby_tickets):
    global pbar
    valid_tickets = []
    for ticket in nearby_tickets:
        valid_ticket = True
        for value in ticket:
            valid = False
            for ranges in rules.values():
                for min_val, max_val in ranges:
                    if min_val <= int(value) <= max_val:
                        valid = True
                        break
                if valid:
                    break

            if not valid:
                valid_ticket = False
                break

        if valid_ticket:
            valid_tickets.append(ticket)

    fields_nb = {field: list(range(len(rules))) for field in rules}
    for ticket in valid_tickets:
        for place, value in enumerate(ticket):
            for field, ranges in rules.items():
                valid = False
                for min_val, max_val in ranges:
                    if min_val <= int(value) <= max_val:
                        valid = True
                        break

                if not valid:
                    try:
                        fields_nb[field].remove(place)
                    except ValueError:
                        pass

    sorted_keys_by_nb_possibilities = sorted([(key, len(values)) for key, values in fields_nb.items()],
                                             key=lambda x: x[1])
    list_keys = [val[0] for val in sorted_keys_by_nb_possibilities]
    position_keys = search(deepcopy([]), deepcopy(fields_nb), list_keys, 0)
    return {list_keys[i]: position_keys[i] for i in range(len(list_keys))}


def search(solution, fields, keys_list, current_key_idx):
    if len(solution) == len(keys_list):
        return solution
    else:
        values_to_try = fields[keys_list[current_key_idx]]
        while len(values_to_try) > 0:
            # Try out a choice
            local_fields = deepcopy(fields)
            choice = values_to_try.pop(0)
            local_fields[keys_list[current_key_idx]] = [choice]
            for key in fields:
                if key != keys_list[current_key_idx]:
                    try:
                        local_fields[key].remove(choice)
                    except ValueError:
                        pass

            # check invalid solution
            if any(len(val) == 0 for val in local_fields.values()):
                continue

            # if solution is valid so far, go to the next field to try
            current_solution = search(solution + [choice],
                                      local_fields,
                                      keys_list,
                                      current_key_idx + 1)
            if current_solution:
                return current_solution

        return False


def get_output(ticket, fields_positions):
    acc = 1
    for key, position in fields_positions.items():
        if 'departure' in key:
            print(key)
            acc = acc * ticket[position]
    return acc


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        inputs = parse_input(f)
        fields_positions = deduce_fields_nb(*inputs)
        print(fields_positions)
        print(get_output(inputs[1], fields_positions))


if __name__ == "__main__":
    main()
