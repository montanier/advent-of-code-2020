#!/usr/bin/python
import argparse
import numpy as np
import operator
import functools


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def count_trees(df):
    # scale df
    while df.shape[1] < df.shape[0] * 7:
        df = np.hstack([df, df])
    nb_lines = df.shape[0]

    # prepare slopes to check
    slopes_to_check = [(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)]
    pos_to_check = []
    for slope in slopes_to_check:
        pos_to_check.append([(i * slope[0], i * slope[1]) for i in range(nb_lines)])

    # count trees for each slope
    res_by_slope = []
    for slope in pos_to_check:
        nb_trees = 0
        for pos in slope:
            if pos[0] < nb_lines:
                if df[pos] == '#':
                    nb_trees += 1
        res_by_slope.append(nb_trees)

    print(res_by_slope)

    # return result
    return functools.reduce(operator.mul, res_by_slope, 1)


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        data = []
        for line in f.readlines():
            data.append(list(line)[:-1])
        df = np.array(data)
        print(count_trees(df))


if __name__ == "__main__":
    main()
