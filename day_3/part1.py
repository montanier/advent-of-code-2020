#!/usr/bin/python
import argparse
import numpy as np


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def count_trees(df):
    nb_trees = 0
    while df.shape[1] < df.shape[0] * 3:
        df = np.hstack([df, df])
    print(df.shape)
    nb_lines = df.shape[0]
    pos_to_check = [(i, i * 3) for i in range(nb_lines)]

    for pos in pos_to_check:
        if df[pos] == '#':
            nb_trees += 1

    return nb_trees


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        data = []
        for line in f.readlines():
            data.append(list(line)[:-1])
        df = np.array(data)
        print(df.shape)
        print(count_trees(df))


if __name__ == "__main__":
    main()
