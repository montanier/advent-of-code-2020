#!/usr/bin/python
import argparse
from itertools import combinations


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')

    return parser


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        data = map(int, f.readlines())
        for val1, val2 in combinations(data, 2):
            if val1 + val2 == 2020:
                print(val1 * val2)
                return


if __name__ == "__main__":
    main()
