#!/usr/bin/python
import argparse
from itertools import combinations
from operator import mul
from functools import reduce


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')

    return parser


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        data = map(int, f.readlines())
        for vals in combinations(data, 3):
            if sum(vals) == 2020:
                print(reduce(mul, vals, 1))
                return


if __name__ == "__main__":
    main()
