#!/usr/bin/python3
import argparse


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    parsed_rules = {}
    received_messages = []
    parse_rules = True

    for line in f.readlines():
        line = line.strip()
        if line == '':
            parse_rules = False
        elif parse_rules:
            rule_nb, all_rules = line.split(': ')
            rule_nb = int(rule_nb)
            if rule_nb == 0:
                start_rule = all_rules.split(' ')
                start_rule = list(map(int, start_rule))
            else:
                or_rules = all_rules.split(' | ')
                parsed_rules[int(rule_nb)] = [[eval(rule) for rule in or_rule.split(' ')] for or_rule in or_rules]
        else:
            received_messages.append(list(line))

    return start_rule, parsed_rules, received_messages


def is_valid(start_rule, rules, message):
    all_char_valid = True
    curr_char, curr_rule_id = 0, 0
    while curr_rule_id < len(start_rule):
        curr_char, valid = _is_valid(rules, start_rule[curr_rule_id], message, curr_char)
        if valid:
            curr_rule_id += 1
        else:
            all_char_valid = False
            break
    if curr_char != len(message):
        all_char_valid = False
    return all_char_valid


def _is_valid(rules, curr_rule_id, message, start_char):
    or_rules = rules[curr_rule_id]
    one_or_rule_valid = False
    for and_rule in or_rules:
        all_and_rules_valid = True
        curr_char = start_char
        for elem in and_rule:
            if isinstance(elem, str):
                if elem != message[curr_char]:
                    all_and_rules_valid = False
                    break
                else:
                    curr_char += 1
            else:
                local_curr_char, valid = _is_valid(rules, elem, message, curr_char)
                if valid:
                    curr_char = local_curr_char
                else:
                    all_and_rules_valid = False
                    break

        if all_and_rules_valid:
            one_or_rule_valid = True
            break

    return curr_char, one_or_rule_valid


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        start_rule, rules, received_messages = parse_input(f)
        nb_valid = 0
        for message in received_messages:
            if is_valid(start_rule, rules, message):
                nb_valid += 1
        print(nb_valid)


if __name__ == "__main__":
    main()
