#!/usr/bin/python3
import argparse


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    parsed_rules = {}
    received_messages = []
    parse_rules = True

    for line in f.readlines():
        line = line.strip()
        if line == '':
            parse_rules = False
        elif parse_rules:
            rule_nb, all_rules = line.split(': ')
            rule_nb = int(rule_nb)
            or_rules = all_rules.split(' | ')
            parsed_rules[int(rule_nb)] = [[eval(rule) for rule in or_rule.split(' ')] for or_rule in or_rules]
        else:
            received_messages.append(list(line))

    return parsed_rules, received_messages


def is_valid(rules, curr_rule_id, message, start_char):
    if start_char >= len(message):
        return []

    or_rules = rules[curr_rule_id]
    end_chars = []
    for and_rule in or_rules:
        sub_end_chars = [start_char]
        for elem in and_rule:
            if isinstance(elem, str):
                if elem != message[start_char]:
                    return []
                else:
                    return [start_char + 1]
            else:
                new_end_chars = []
                for curr_end_char in sub_end_chars:
                    new_end_chars += is_valid(rules, elem, message, curr_end_char)
                sub_end_chars = new_end_chars
        end_chars += sub_end_chars

    return end_chars


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        rules, received_messages = parse_input(f)
        rules[8] = [[42, 8], [42]]
        rules[11] = [[42, 11, 31], [42, 31]]
        nb_valid = 0
        for message in received_messages:
            if len(message) in is_valid(rules, 0, message, 0):
                nb_valid += 1
        print(nb_valid)


if __name__ == "__main__":
    main()
