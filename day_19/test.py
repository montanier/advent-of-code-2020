import unittest
from part2 import is_valid


class TestValid(unittest.TestCase):

    def test_is_valid_returns_true_on_simple_and(self):
        # Given
        message = list('ab')
        rules = {0: [[1, 2]], 1: [['a']], 2: [['b']]}

        # When
        validity = is_valid(rules, 0, message, 0)

        # Then
        self.assertIn(len(message), validity)

    def test_is_valid_returns_false_on_simple_and(self):
        # Given
        message = list('ab')
        rules = {0: [[2, 1]], 1: [['a']], 2: [['b']]}

        # When
        validity = is_valid(rules, 0, message, 0)

        # Then
        self.assertNotIn(len(message), validity)

    def test_is_valid_returns_true_on_simple_or_on_left_branch(self):
        # Given
        message = list('ab')
        rules = {0: [[1]],
                 1: [[2, 3], [3, 3]],
                 2: [['a']],
                 3: [['b']]}

        # When
        validity = is_valid(rules, 0, message, 0)

        # Then
        self.assertIn(len(message), validity)

    def test_is_valid_returns_true_on_simple_or_on_right_branch(self):
        # Given
        message = list('bb')
        rules = {0: [[1]],
                 1: [[2, 3], [3, 3]],
                 2: [['a']],
                 3: [['b']]}

        # When
        validity = is_valid(rules, 0, message, 0)

        # Then
        self.assertIn(len(message), validity)

    def test_is_valid_returns_false_on_simple_or(self):
        # Given
        message = list('aa')
        rules = {0: [[1]],
                 1: [[2, 3], [3, 3]],
                 2: [['a']],
                 3: [['b']]}

        # When
        validity = is_valid(rules, 0, message, 0)

        # Then
        self.assertNotIn(len(message), validity)

    def test_is_valid_returns_true_on_composed_rules(self):
        # Given
        messages = [list('aba'), list('baa')]
        rules = {0: [[1, 2]], 1: [[2, 3], [3, 2]], 2: [['a']], 3: [['b']]}

        # When
        validities = [(lambda x: len(message) in x)(is_valid(rules, 0, message, 0)) for message in messages]

        # Then
        self.assertEqual(all(validities), True)

    def test_is_valid_returns_expected_true_on_loop(self):
        # Given
        messages = [list('aabb'),
                    list('aaaaaaab'),
                    list('aaaaaaaaabab'),
                    list('aaaaaaaaaaababab')]
        rules = {0: [[1, 2]],
                 1: [[3, 3], [4, 4]],
                 2: [[1, 2, 5], [1]],
                 3: [['a']],
                 4: [['b']],
                 5: [[3, 4]]}

        # When
        validities = [(lambda x: len(message) in x)(is_valid(rules, 0, message, 0)) for message in messages]

        # Then
        self.assertEqual(all(validities), True)

    def test_is_valid_returns_expected_false_on_loop(self):
        # Given
        messages = [list('ab'),
                    list('aaab'),
                    list('bbab'),
                    list('bbba')]
        rules = {0: [[1, 2]],
                 1: [[3, 4], [4, 3]],
                 2: [[1, 2], [1]],
                 3: [['a']],
                 4: [['b']]}

        # When
        validities = [(lambda x: len(message) in x)(is_valid(rules, 0, message, 0)) for message in messages]

        # Then
        self.assertEqual(any(validities), False)

    def test_is_valid_returns_expected_true_on_complex_example(self):
        # Given
        messages = [list('aaaabb'),
                    list('aaabab'),
                    list('abbabb'),
                    list('abbbab'),
                    list('aabaab'),
                    list('aabbbb'),
                    list('abaaab'),
                    list('ababbb')]
        rules = {0: [[4, 1, 5]],
                 1: [[2, 3], [3, 2]],
                 2: [[4, 4], [5, 5]],
                 3: [[4, 5], [5, 4]],
                 4: [['a']],
                 5: [['b']]}

        # When
        validities = [(lambda x: len(message) in x)(is_valid(rules, 0, message, 0)) for message in messages]

        # Then
        self.assertEqual(all(validities), True)


if __name__ == '__main__':
    unittest.main()
