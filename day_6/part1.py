#!/usr/bin/python3
import argparse


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        nb_answers = []
        current_answers = set()
        for line in f.readlines():
            if line == '\n':
                nb_answers.append(len(current_answers))
                current_answers = set()
            else:
                current_answers = current_answers.union(list(line.rstrip('\n')))
        nb_answers.append(len(current_answers))
        print(sum(nb_answers))


if __name__ == "__main__":
    main()
