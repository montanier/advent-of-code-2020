#!env python3
import argparse

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    lines = f.readlines()
    time = int(lines[0].strip())
    busses = lines[1].strip().split(',')
    busses = filter(lambda x: x != 'x', busses)
    busses = list(map(int, busses))
    return time, busses


def find_bus(time, busses):
    min_time = None
    best_bus = 0
    for bus in busses:
        next_pass = int(time / bus) * bus
        if time % bus != 0:
            next_pass += bus
        if (not min_time) or (min_time and next_pass < min_time):
            min_time = next_pass
            best_bus = bus
    return (min_time - time) * best_bus


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        time, busses = parse_input(f)
        print(find_bus(time, busses))


if __name__ == "__main__":
    main()
