#!/usr/bin/python3
import argparse
from itertools import permutations

# 5 for test file, 25 for input
size_max_buff = 25


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    stream = []
    for line in f.readlines():
        stream.append(int(line.strip()))
    return stream

def find_broken(stream):
    local_buff = []
    for val in stream:
        local_buff.append(val)
        if len(local_buff) > size_max_buff:
            for a, b in permutations(local_buff[0:size_max_buff], 2):
                if a + b == local_buff[size_max_buff]:
                    break
            else:
                return val

            local_buff.pop(0)


def find_range(stream, broken):
    first_id = 0
    last_id = 1
    sum_range = sum(stream[first_id:last_id + 1])
    while sum_range != broken:
        if sum_range < broken:
            last_id += 1
        else:
            first_id += 1
        sum_range = sum(stream[first_id:last_id + 1])

    return first_id, last_id


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        stream = parse_input(f)
        broken = find_broken(stream)
        print(broken)
        range_idx = find_range(stream, broken)
        print(range_idx)
        range_vals = stream[range_idx[0]: range_idx[1] + 1]
        print(range_vals)
        print(min(range_vals) + max(range_vals))


if __name__ == "__main__":
    main()
