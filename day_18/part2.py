#!/usr/bin/python3
import argparse
import operator


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    expressions = []
    for line in f.readlines():
        expressions.append(line.strip())
    return expressions


def eval_expression(expression):
    expression = _eval_expression(list(expression), 0, '+')
    expression = _eval_expression(list(expression), 0, '*')
    return expression


def _apply_operator(curr_operator, left_element, right_element, expression, ptr):
    left_val = int("".join(left_element))
    right_val = int("".join(right_element))
    start_pos = ptr - 3 - len(left_element) - len(right_element)
    expression[start_pos:ptr] = list(str(curr_operator(left_val, right_val)))
    return expression, start_pos


def _eval_expression(expression, ptr, target_operator):
    right_element = []
    left_element = []
    curr_operator = None
    while ptr < len(expression):
        curr_char = expression[ptr]

        if curr_char == ' ':
            if curr_operator and right_element:
                expression, ptr = _apply_operator(curr_operator, left_element, right_element, expression, ptr)
                curr_operator = None
                left_element = []
                right_element = []
                continue

        elif curr_char == target_operator:
            if curr_char == '+':
                curr_operator = operator.add
            else:
                curr_operator = operator.mul

        elif curr_char == '(':
            expression = _eval_expression(expression, ptr + 1, '+')
            expression = _eval_expression(expression, ptr + 1, '*')
            ptr_end_parenthesis = ptr
            while expression[ptr_end_parenthesis] != ')':
                ptr_end_parenthesis += 1
            expression[ptr:ptr_end_parenthesis + 1] = list(expression[ptr + 1:ptr_end_parenthesis])
            continue

        elif curr_char == ')':
            break

        elif '0' <= curr_char <= '9':
            if curr_operator is None:
                left_element.append(curr_char)
            else:
                right_element.append(curr_char)
        else:
            curr_operator = None
            left_element = []
            right_element = []

        ptr += 1

    if curr_operator and right_element:
        expression, ptr = _apply_operator(curr_operator, left_element, right_element, expression, ptr)

    return expression


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        expressions = parse_input(f)
        sum_eval = 0
        for expression in expressions:
            res = eval_expression(expression)
            res_val = int(''.join(res))
            # print(res_val)
            sum_eval += res_val
        print(sum_eval)


if __name__ == "__main__":
    main()
