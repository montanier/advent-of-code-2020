#!/usr/bin/python3
import argparse
import operator


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    expressions = []
    for line in f.readlines():
        expressions.append(line.strip())
    return expressions


def eval_expression(expression):
    return _eval_expression(list(expression), 0)


def _apply_operator(curr_operator, curr_element, acc):
    if len(curr_element) > 0:
        val = int("".join(curr_element))
        if curr_operator:
            acc = curr_operator(acc, val)
        else:
            acc = val
    return acc


def _eval_expression(expression, ptr):
    acc = 0
    curr_element = []
    curr_operator = None
    while ptr < len(expression):
        curr_char = expression[ptr]

        if curr_char == ' ':
            acc = _apply_operator(curr_operator, curr_element, acc)
            curr_element = []

        elif curr_char == '(':
            ptr, local_acc = _eval_expression(expression, ptr + 1)
            if curr_operator:
                acc = curr_operator(acc, local_acc)
            else:
                acc = local_acc

        elif curr_char == ')':
            acc = _apply_operator(curr_operator, curr_element, acc)
            if ptr < len(expression) - 1 and expression[ptr + 1] == ' ':
                ptr += 1
            return ptr, acc

        elif curr_char == '+':
            curr_operator = operator.add
            expression.pop(0)
        elif curr_char == '*':
            curr_operator = operator.mul
            expression.pop(0)
        else:
            curr_element.append(curr_char)
        ptr += 1

    acc = _apply_operator(curr_operator, curr_element, acc)

    return None, acc




def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        expressions = parse_input(f)
        sum_eval = 0
        for expression in expressions:
            _, res = eval_expression(expression)
            print(res)
            sum_eval += res
        print(sum_eval)


if __name__ == "__main__":
    main()
