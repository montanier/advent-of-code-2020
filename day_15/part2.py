#!env python3
import argparse
from tqdm import tqdm

max_step = 30000000 - 1

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    game = {}
    line = f.readlines()[0]
    last_val = 0
    for i, value in enumerate(line.strip().split(',')[:-1]):
        game[int(value)] = i
    last_val = int(line.strip().split(',')[-1])
    return game, last_val


def run_game(game, last_val):
    step = len(game)
    pbar = tqdm(total=max_step)
    pbar.update(step)
    while step < max_step:
        last_time = game.get(last_val, -1)
        game[last_val] = step
        if last_time != -1:
            last_val = step - last_time
        else:
            last_val = 0
        step += 1
        pbar.update(1)
    return game, last_val


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        inputs = parse_input(f)
        game, last_val = run_game(*inputs)
        print(last_val)


if __name__ == "__main__":
    main()
