#!env python3
import argparse
import numpy as np

nghb_rel = [(x, y) for x in range(-1, 2) for y in range(-1, 2)]
nghb_rel.remove((0, 0))

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    layout = []
    for line in f.readlines():
        layout.append(list(line.strip()))
    layout = np.array(layout)
    return layout


def step_simu(layout):
    new_layout = layout.copy()
    for x in range(layout.shape[0]):
        for y in range(layout.shape[1]):
            if layout[x, y] == '.':
                continue

            cpt_used = 0
            all_free = True
            for rel_x, rel_y in nghb_rel:
                check_x = x + rel_x
                check_y = y + rel_y

                if (0 <= check_x < layout.shape[0] and 0 <= check_y < layout.shape[1]):
                    if layout[check_x, check_y] == '#':
                        all_free = False
                        cpt_used += 1

            if layout[x, y] == 'L' and all_free:
                new_layout[x, y] = '#'

            if layout[x, y] == '#' and cpt_used >= 4:
                new_layout[x, y] = 'L'

    occupied_seat = 0
    for x in range(layout.shape[0]):
        for y in range(layout.shape[1]):
            if new_layout[x, y] == '#':
                occupied_seat += 1

    return new_layout, occupied_seat


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        layout = parse_input(f)
        prev_occupied_seat = 0
        layout, occupied_seat = step_simu(layout)
        while prev_occupied_seat != occupied_seat:
            prev_occupied_seat = occupied_seat
            layout, occupied_seat = step_simu(layout)
        print(occupied_seat)


if __name__ == "__main__":
    main()
