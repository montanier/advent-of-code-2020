#!env python3
import argparse
import numpy as np

nghb_dir = [(x, y) for x in range(-1, 2) for y in range(-1, 2)]
nghb_dir.remove((0, 0))

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    layout = []
    for line in f.readlines():
        layout.append(list(line.strip()))
    layout = np.array(layout)
    return layout


def step_simu(layout):
    new_layout = layout.copy()
    for x in range(layout.shape[0]):
        for y in range(layout.shape[1]):
            if layout[x, y] == '.':
                continue

            cpt_used = 0
            all_free = True
            for rel_x, rel_y in nghb_dir:
                length_vec = 1
                while True:
                    check_x = x + length_vec * rel_x
                    check_y = y + length_vec * rel_y

                    if check_x < 0 or check_y < 0 or check_x >= layout.shape[0] or check_y >= layout.shape[1]:
                        break
                    else:
                        if layout[check_x, check_y] == '#':
                            all_free = False
                            cpt_used += 1
                            break

                        if layout[check_x, check_y] == 'L':
                            break

                    length_vec += 1

            if layout[x, y] == 'L' and all_free:
                new_layout[x, y] = '#'

            if layout[x, y] == '#' and cpt_used >= 5:
                new_layout[x, y] = 'L'

    occupied_seat = 0
    for x in range(layout.shape[0]):
        for y in range(layout.shape[1]):
            if new_layout[x, y] == '#':
                occupied_seat += 1

    return new_layout, occupied_seat


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        layout = parse_input(f)
        prev_occupied_seat = 0
        layout, occupied_seat = step_simu(layout)
        while prev_occupied_seat != occupied_seat:
            prev_occupied_seat = occupied_seat
            layout, occupied_seat = step_simu(layout)
        print(occupied_seat)


if __name__ == "__main__":
    main()
