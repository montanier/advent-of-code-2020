#!/usr/bin/python3
import argparse


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    stream = []
    for line in f.readlines():
        stream.append(int(line.strip()))
    return stream


def find_adaptators(stream):
    sorted_stream = sorted(stream)
    nb_1_diff = 0
    nb_3_diff = 0
    for id in range(len(sorted(sorted_stream))):
        if id == 0:
            diff = sorted_stream[id]
        else:
            diff = sorted_stream[id] - sorted_stream[id - 1]
        if diff == 1:
            nb_1_diff += 1
        if diff == 3:
            nb_3_diff += 1

    # add diff to device
    nb_3_diff += 1
    print(f'{nb_1_diff} {nb_3_diff}')
    return nb_1_diff * nb_3_diff


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        stream = parse_input(f)
        print(find_adaptators(stream))


if __name__ == "__main__":
    main()
