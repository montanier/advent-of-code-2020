#!/usr/bin/python3
import argparse


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    stream = []
    for line in f.readlines():
        stream.append(int(line.strip()))
    return stream


def find_options(stream):
    stream += [0]
    sorted_stream = sorted(stream)
    sorted_stream.append(sorted_stream[-1] + 3)

    print(sorted_stream)
    nb_options = [0] * len(sorted_stream)
    nb_options[0] = 1

    print(nb_options)

    for id in range(len(sorted_stream) - 1):
        next_id = id + 1
        diff = sorted_stream[next_id] - sorted_stream[id]
        while diff <= 3:
            nb_options[next_id] += nb_options[id]
            next_id += 1
            if next_id < len(sorted_stream):
                diff = sorted_stream[next_id] - sorted_stream[id]
            else:
                break
        print(nb_options)

    return(nb_options[-1])


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        stream = parse_input(f)
        print(find_options(stream))


if __name__ == "__main__":
    main()
