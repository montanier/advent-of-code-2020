#!env python3
import argparse
import numpy as np

nghb_rel = [(z, x, y) for x in range(-1, 2) for y in range(-1, 2) for z in range(-1, 2)]
nghb_rel.remove((0, 0, 0))

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    layout = []
    for line in f.readlines():
        layout.append(list(line.strip()))
    layout = np.array([layout])
    return layout


def step_simu(layout):
    # expend
    expended_layout = layout.copy()
    empty_layer = np.array(
        [[['.' for i in range(expended_layout.shape[1])] for j in range(expended_layout.shape[2])]]
    ).reshape(
        (1, expended_layout.shape[1], expended_layout.shape[2])
    )
    expended_layout = np.concatenate((empty_layer, expended_layout, empty_layer), axis=0)

    empty_layer = np.array(
        [[['.' for i in range(expended_layout.shape[0])] for j in range(expended_layout.shape[2])]]
    ).reshape(
        (expended_layout.shape[0], 1, expended_layout.shape[2])
    )
    expended_layout = np.concatenate((empty_layer, expended_layout, empty_layer), axis=1)

    empty_layer = np.array(
        [[['.' for i in range(expended_layout.shape[0])] for j in range(expended_layout.shape[1])]]
    ).reshape(
        (expended_layout.shape[0], expended_layout.shape[1], 1)
    )
    expended_layout = np.concatenate((empty_layer, expended_layout, empty_layer), axis=2)

    # apply rule
    new_layout = expended_layout.copy()
    for z in range(expended_layout.shape[0]):
        for x in range(expended_layout.shape[1]):
            for y in range(expended_layout.shape[2]):
                cpt_active = 0
                for rel_z, rel_x, rel_y in nghb_rel:
                    check_z = z + rel_z
                    check_x = x + rel_x
                    check_y = y + rel_y

                    if (0 <= check_z < expended_layout.shape[0]) and\
                            (0 <= check_x < expended_layout.shape[1]) and\
                            (0 <= check_y < expended_layout.shape[2]):
                        if expended_layout[check_z, check_x, check_y] == '#':
                            cpt_active += 1

                if expended_layout[z, x, y] == '.' and cpt_active == 3:
                    new_layout[z, x, y] = '#'
                if expended_layout[z, x, y] == '#' and (cpt_active != 3 and cpt_active != 2):
                    new_layout[z, x, y] = '.'

    # trim
    min_z, max_z = 0, expended_layout.shape[0]
    min_x, max_x = 0, expended_layout.shape[1]
    min_y, max_y = 0, expended_layout.shape[2]
    for z in range(expended_layout.shape[0]):
        if np.any(new_layout[z, :, :] == '#'):
            min_z = z
            break
    for z in range(expended_layout.shape[0] - 1, min_z, -1):
        if np.any(new_layout[z, :, :] == '#'):
            max_z = z
            break

    for x in range(expended_layout.shape[1]):
        if np.any(new_layout[:, x, :] == '#'):
            min_x = x
            break
    for x in range(expended_layout.shape[1] - 1, min_x, -1):
        if np.any(new_layout[:, x, :] == '#'):
            max_x = x
            break

    for y in range(expended_layout.shape[2]):
        if np.any(new_layout[:, :, y] == '#'):
            min_y = y
            break
    for y in range(expended_layout.shape[2] - 1, min_y, -1):
        if np.any(new_layout[:, :, y] == '#'):
            max_y = y
            break
    new_layout = new_layout[range(min_z, max_z + 1), :, :]
    new_layout = new_layout[:, range(min_x, max_x + 1), :]
    new_layout = new_layout[:, :, range(min_y, max_y + 1)]

    return new_layout


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        layout = parse_input(f)
        for cycle in range(6):
            layout = step_simu(layout)
            print(np.unique(layout, return_counts=True))


if __name__ == "__main__":
    main()
