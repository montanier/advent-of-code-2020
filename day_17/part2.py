#!env python3
import argparse
import numpy as np

nb_dim = 4

nghb_rel = []
for i in range(nb_dim):
    value = [j for j in range(-1, 2)]
    if len(nghb_rel) == 0:
        nghb_rel = [[val] for val in value]
    else:
        nghb_rel = [curr + [val] for curr in nghb_rel for val in value]
nghb_rel.remove([0] * nb_dim)


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    layout = []
    for line in f.readlines():
        layout.append(list(line.strip()))
    for i in range(2, nb_dim):
        layout = [layout]
    layout = np.array(layout)
    return layout


def grid_positions(shape):
    positions = []
    for i in range(nb_dim):
        value = [j for j in range(shape[i])]
        if len(positions) == 0:
            positions = [[val] for val in value]
        else:
            positions = [curr + [val] for curr in positions for val in value]
    return positions


def pos_in_grid(pos, shape):
    for dim in range(nb_dim):
        if not 0 <= pos[dim] < shape[dim]:
            return False
    return True


def step_simu(layout):
    # expend
    expended_layout = layout.copy()
    for dim in range(nb_dim):
        empty_layer = []
        reshape_dims = []
        for i in range(nb_dim):
            if i != dim:
                reshape_dims.append(expended_layout.shape[i])
                if len(empty_layer) == 0:
                    empty_layer = ['.' for j in range(expended_layout.shape[i])]
                else:
                    empty_layer = [empty_layer for j in range(expended_layout.shape[i])]
            else:
                reshape_dims.append(1)
        empty_layer = np.array(empty_layer).reshape(reshape_dims)
        expended_layout = np.concatenate((empty_layer, expended_layout, empty_layer), axis=dim)

    # apply rule
    new_layout = expended_layout.copy()
    positions = grid_positions(expended_layout.shape)
    for position in positions:
        cpt_active = 0
        for rel in nghb_rel:
            check_pos = [sum(x) for x in zip(rel, position)]

            if pos_in_grid(check_pos, expended_layout.shape):
                if expended_layout[tuple(check_pos)] == '#':
                    cpt_active += 1

        if expended_layout[tuple(position)] == '.' and cpt_active == 3:
            new_layout[tuple(position)] = '#'
        if expended_layout[tuple(position)] == '#' and (cpt_active != 3 and cpt_active != 2):
            new_layout[tuple(position)] = '.'

    # trim
    for dim in range(nb_dim):
        for i in range(expended_layout.shape[dim]):
            slc = [slice(None)] * nb_dim
            slc[dim] = i
            if np.any(new_layout[tuple(slc)] == '#'):
                min_on_axis = i
                break

        for i in range(expended_layout.shape[dim] - 1, min_on_axis, -1):
            slc = [slice(None)] * nb_dim
            slc[dim] = i
            if np.any(new_layout[tuple(slc)] == '#'):
                max_on_axis = i
                break

        slc = [slice(None)] * nb_dim
        slc[dim] = slice(min_on_axis, max_on_axis + 1)
        new_layout = new_layout[tuple(slc)]

    return new_layout


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        layout = parse_input(f)
        for cycle in range(6):
            layout = step_simu(layout)
            print(np.unique(layout, return_counts=True))


if __name__ == "__main__":
    main()
