#!/usr/bin/python3
import argparse
import re


pattern_contained = re.compile('([0-9]+) ([\w ]+) bag')

known_nodes = {}


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


class Node:
    def __init__(self, color):
        self.color = color
        self.is_contained_by = []
        self.contains = []

    def add_contains(self, node, nb):
        self.contains.append((int(nb), node))
        node.is_contained_by.append(self)


def build_input_tree(f):
    for line in f.readlines():
        line = line.rstrip('\n')
        outside, inside = line.split('bags contain')
        outside = outside.rstrip(' ')

        if outside not in known_nodes:
            outside_node = Node(outside)
            known_nodes[outside] = outside_node
        else:
            outside_node = known_nodes[outside]

        for contained in inside.split(', '):
            contained = contained.lstrip(' ').rstrip('s.')
            if contained != 'no other bag':
                match = pattern_contained.match(contained)
                assert match, contained
                color = match.group(2)
                nb = match.group(1)

                if color not in known_nodes:
                    contained_node = Node(color)
                    known_nodes[color] = contained_node
                else:
                    contained_node = known_nodes[color]

                outside_node.add_contains(contained_node, nb)


def find_containers(key):
    root = known_nodes[key]
    containers_node = []
    to_explore = root.is_contained_by
    while len(to_explore) > 0:
        current_node = to_explore.pop(0)
        if current_node.color not in containers_node:
            containers_node.append(current_node.color)
            to_explore += current_node.is_contained_by
    return containers_node


def find_contains(key):
    root = known_nodes[key]
    if len(root.contains) == 0:
        return 0
    to_explore = root.contains
    acc = 0
    for nb_bags, color_node in to_explore:
        color_bag = color_node.color
        color_contains = find_contains(color_bag)
        acc += nb_bags + (nb_bags * color_contains)
        print(f"{nb_bags} {color_bag} {color_contains} {acc}")
    return acc


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        build_input_tree(f)
        print(find_contains('shiny gold'))


if __name__ == "__main__":
    main()
