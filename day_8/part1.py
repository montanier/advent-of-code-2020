#!/usr/bin/python3
import argparse

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    programm = []
    for line in f.readlines():
        line = line.rstrip('\n')
        instr, val = line.split(' ')
        sign_val = list(val)[0]
        if sign_val == '+':
            val = int(''.join(list(val)[1:]))
        if sign_val == '-':
            val = -int(''.join(list(val)[1:]))
        programm.append((instr, val))
    return programm


def find_loop(programm):
    acc = 0
    instr_ptr = 0
    seen_instr = []
    while instr_ptr not in seen_instr:
        seen_instr.append(instr_ptr)
        instr, val = programm[instr_ptr]
        if instr == 'nop':
            instr_ptr += 1
        elif instr == 'acc':
            acc += val
            instr_ptr += 1
        elif instr == 'jmp':
            instr_ptr += val
    return acc


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        programm = parse_input(f)
        print(programm)
        print(find_loop(programm))


if __name__ == "__main__":
    main()
