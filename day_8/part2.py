#!/usr/bin/python3
import argparse
from copy import copy

def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(f):
    programm = []
    for line in f.readlines():
        line = line.rstrip('\n')
        instr, val = line.split(' ')
        sign_val = list(val)[0]
        if sign_val == '+':
            val = int(''.join(list(val)[1:]))
        if sign_val == '-':
            val = -int(''.join(list(val)[1:]))
        programm.append((instr, val))
    return programm


def find_loop(programm):
    acc = 0
    instr_ptr = 0
    seen_instr = []
    while instr_ptr not in seen_instr:
        if instr_ptr > len(programm) - 1:
            return False, acc

        seen_instr.append(instr_ptr)
        instr, val = programm[instr_ptr]
        if instr == 'nop':
            instr_ptr += 1
        elif instr == 'acc':
            acc += val
            instr_ptr += 1
        elif instr == 'jmp':
            instr_ptr += val

    return True, acc


def find_modif(programm):
    for instr_uid in range(len(programm)):
        instr, val = programm[instr_uid]
        if instr == 'nop':
            local_prog = copy(programm)
            local_prog[instr_uid] = ('jmp', val)
            loop, acc = find_loop(local_prog)
            if not loop:
                print(instr_uid)
                return acc
        if instr == 'jmp':
            local_prog = copy(programm)
            local_prog[instr_uid] = ('nop', val)
            loop, acc = find_loop(local_prog)
            if not loop:
                print(instr_uid)
                return acc


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        programm = parse_input(f)
        print(find_modif(programm))


if __name__ == "__main__":
    main()
