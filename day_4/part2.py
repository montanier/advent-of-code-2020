#!/usr/bin/python3
import argparse
from copy import copy
import re

mandatory_keys = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(file_lines):
    passports = []
    new_passport = {}
    for line in file_lines:
        if line == "\n":
            passports.append(copy(new_passport))
            new_passport = {}
        else:
            for elem in line.split(" "):
                key, value = elem.split(':')
                new_passport[key] = value.rstrip('\n')
    passports.append(new_passport)
    return passports


def valid_values(passport):
    if len(passport['byr']) != 4:
        return False
    if int(passport['byr']) < 1920:
        return False
    if int(passport['byr']) > 2002:
        return False

    if len(passport['iyr']) != 4:
        return False
    if int(passport['iyr']) < 2010:
        return False
    if int(passport['iyr']) > 2020:
        return False

    if len(passport['eyr']) != 4:
        return False
    if int(passport['eyr']) < 2020:
        return False
    if int(passport['eyr']) > 2030:
        return False

    if passport['hgt'].endswith('cm'):
        height = int(passport['hgt'].rstrip('cm'))
        if height < 150:
            return False
        if height > 193:
            return False
    elif passport['hgt'].endswith('in'):
        height = int(passport['hgt'].rstrip('in'))
        if height < 59:
            return False
        if height > 76:
            return False
    else:
        return False

    hcl_pattern = re.compile('#[0-9a-f]{6}')
    if not hcl_pattern.fullmatch(passport['hcl']):
        return False

    ecl_valid = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
    if not passport['ecl'] in ecl_valid:
        return False

    if len(passport['pid']) != 9:
        return False

    return True

def count_passport(passports):
    nb_passports = 0
    for passport in passports:
        if all(key in passport for key in mandatory_keys):
            if (valid_values(passport)):
                nb_passports += 1
    return nb_passports


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        data = []
        for line in f.readlines():
            data.append(line)
        passports = parse_input(data)
        print(count_passport(passports))


if __name__ == "__main__":
    main()
