#!/usr/bin/python3
import argparse
from copy import copy
import re

mandatory_keys = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']


def init_argparse():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    return parser


def parse_input(file_lines):
    passports = []
    new_passport = {}
    for line in file_lines:
        if line == "\n":
            passports.append(copy(new_passport))
            new_passport = {}
        else:
            for elem in line.split(" "):
                key, value = elem.split(':')
                new_passport[key] = value.rstrip('\n')
    passports.append(new_passport)
    return passports


def count_passport(passports):
    nb_passports = 0
    for passport in passports:
        if all(key in passport for key in mandatory_keys):
            nb_passports += 1
    return nb_passports


def main():
    parser = init_argparse()
    args = parser.parse_args()
    with open(args.file, "r") as f:
        data = []
        for line in f.readlines():
            data.append(line)
        passports = parse_input(data)
        print(count_passport(passports))


if __name__ == "__main__":
    main()
